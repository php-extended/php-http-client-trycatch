<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-trycatch library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\TryCatchClient;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;

/**
 * TryCatchClientTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\TryCatchClient
 *
 * @internal
 *
 * @small
 */
class TryCatchClientTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var TryCatchClient
	 */
	protected TryCatchClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new TryCatchClient(
			$this->getMockForAbstractClass(ClientInterface::class),
			$this->getMockForAbstractClass(ClientInterface::class),
		);
	}
	
}
