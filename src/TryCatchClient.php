<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-trycatch library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * TryCatchClient class file.
 * 
 * This represents a client that tries multiple canals to reach its target.
 * 
 * @author Anastaszor
 */
class TryCatchClient implements ClientInterface, Stringable
{
	
	/**
	 * The client that is used by default.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_defaultClient;
	
	/**
	 * The client that is used if the first client fails.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_elseClient;
	
	/**
	 * Builds a new TryCatchClient with the given default and else clients.
	 * 
	 * @param ClientInterface $defaultClient
	 * @param ClientInterface $elseClient
	 */
	public function __construct(ClientInterface $defaultClient, ClientInterface $elseClient)
	{
		$this->_defaultClient = $defaultClient;
		$this->_elseClient = $elseClient;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		try
		{
			return $this->_defaultClient->sendRequest($request);
		}
		catch(ClientExceptionInterface $e)
		{
			return $this->_elseClient->sendRequest($request);
		}
	}
	
}
