# php-extended/php-http-client-trycatch

A client that tries to get a response from a client, but tries to get it from the other if the first fail.

![coverage](https://gitlab.com/php-extended/php-http-client-trycatch/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-trycatch/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-trycatch ^8`


## Basic Usage

This library may be used the following way :

```php

/* @var $defaultClient Psr\Http\Client\ClientInterface */    // psr-18
/* @var $elseClient    Psr\Http\Client\ClientInterface */    // psr-18
/* @var $client        Psr\Http\Client\ClientInterface */    // psr-18

$client = new TryCatchClient($defaultClient, $elseClient);
$response = $client->sendRequest($request);

/* @var $response Psr\Http\Message\ResponseInterface */
// if $defaultClient throws, uses $elseClient
// if $elseClient throws, then throws that second exception

```


## License

MIT (See [license file](LICENSE)).
